import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotoFormComponent } from './photo-form.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [ PhotoFormComponent ],
    imports: [
        CommonModule,
        HttpClientModule
    ]
})
export class PhotoFormModule { }